import Moment from 'react-moment';
import React from "react";

export const getNumberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export const getDate = (date = new Date()) =>{
    return <Moment format="YYYY-MM-DD">{date}</Moment>;
}

export const getDaysFromNow = (date) => {
   return <Moment fromNow>{date}</Moment>
}