import "./App.css";
import ROUTES, { RenderRoutes } from "../../routes";
import Header from "../Header";
import Template from "../generics/Template";

function App() {
  return (
    <Template>
      <RenderRoutes routes={ROUTES} />
    </Template>
  );
}

export default App;
