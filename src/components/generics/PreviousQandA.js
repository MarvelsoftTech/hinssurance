import React from "react";

function PreviousQandA({question,answer}) {
  return (
    <div className="mb-10">
      <h1 className="text-gray-lightest text-xl">
        {question}
      </h1>
      <h1 className="text-gray-lightest text-xl font-bold">{answer}</h1>
    </div>
  );
}

export default PreviousQandA;
