import React from "react";
import Header from "../Header";

function Template(props) {
  return (
      <>
      <Header/>
    <div className="flex">
      <div className="w-9/12 h-auto">{props.children}</div>
      <div class="w-3/12 bg-pink-dark min-h-screen"></div>
    </div>
    </>
  );
}

export default Template;
