import React from "react";
import { Route, Switch } from "react-router-dom";
import MotorInssuranceFlow from "./pages/MotorInssuranceFlow";
import SelectInssurance from "./pages/SelectInssurance";
import RegistrationFlow from "./pages/RegistrationFlow"




const ROUTES = [
  { path: "/", key: "ROOT", exact: true, component: () => <SelectInssurance /> },
  {
      path:"/plans",
      key:"PLANS",
      component:RenderRoutes,
      routes:[
        {
            path: "/plans/motor",
            key: "PLANS",
            exact: true,
            component: () => <MotorInssuranceFlow />,
          }
      ]
  },
  {
    path:"/kyc",
    key:"KYC",
    component:RenderRoutes,
    routes:[
      {
          path: "/kyc/motor",
          key: "KYC",
          exact: true,
          component: () => <RegistrationFlow />,
        }
    ]
},
];
export default ROUTES;

function RouteWithSubRoutes(route) {
  return (
    <Route
      path={route.path}
      exact={route.exact}
      render={(props) => <route.component {...props} routes={route.routes} />}
    />
  );
}

export function RenderRoutes({ routes }) {
  return (
    <>
      <Switch>
        {routes.map((route, i) => {
          return <RouteWithSubRoutes key={route.key} {...route} />;
        })}
        <Route component={() => <h1>Not Found!</h1>} />
      </Switch>
    </>
  );
}
