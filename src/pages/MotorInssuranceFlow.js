import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
// import { getDate } from '../utilities/Functions'

function MotorInssuranceFlow(props) {
  const [step, setStep] = useState(2);
  const [addressInput, setAddressInput] = useState(false);
  const [carDetails, setCarDetails] = useState({
    motorClass: "",
    motorType: "",
    motorValue: "",
    quotedPremium: "",
    videoVerification: "",
  });
  const [coverType, setCoverType] = useState("");
  const [stepList, setStepList] = useState([1, 2]);

  const nextStep = () => {
    setStep(step + 1);
    setStepList([...stepList, stepList[stepList.length - 1] + 1]);
  };

  const previousStep = () => {
    setStep(step - 1);
    const newStepList = stepList.filter((step) => step !== stepList.length);
    setStepList(newStepList);
  };

  const getCarDetails = (e) => {
    let { name, value } = e.target;
    setCarDetails({ ...carDetails, [name]: value });
    console.log(carDetails);
  };

  const geoLocationFunction = () => {};

  const nextFormField = (e) => {
    e.preventDefault();
    nextStep();
  };

  return (
    <>
      <div className="h-24 w-full bg-pink-light flex items-center">
        <h6 className="text-heirsred h-5 m-0 pl-20">Buy Insurance</h6>
      </div>
      <div className="ml-20 mt-16">
        <div className="flex">
          <div>
            <div
              className="h-14 w-14 rounded-full bg-white shadow-sm flex items-center justify-center cursor-pointer"
              onClick={step > 2 ? previousStep : ""}
            >
              <img src="/assets/Back Arrow.png" alt />
            </div>
            <div className="h-24 border-l w-0 ml-7 my-2 border-black-dim" />
            <ul className="ml-5">
              {stepList.map((sl) => (
                <li
                  className={`rounded-full border ${
                    step == sl
                      ? "border-heirsred"
                      : "border-black-dim bg-pink-light"
                  } text-heirsred text-xsm h-4 w-4 flex items-center justify-center mb-4 cursor-pointer`}
                  onClick={() => {
                    setStepList(stepList.splice(0, sl));
                    setStep(sl);
                  }}
                >
                  {sl}
                </li>
              ))}
            </ul>
          </div>
          {step == 2 ? (
            <div className="ml-20 w-full relative ">
              <div className="mb-20">
                <h1 className="text-gray-lightest text-xl w-full">
                  Select Type of Cover
                </h1>
                <h1 className="text-gray-lightest text-xl font-bold w-full">
                  3rd Party
                </h1>
              </div>
              <div className="absolute w-60 h-50 rounded-md bg-black-dim px-5 py-2 tools top-0 left-1/4">
                <div className="tool-tip z-0" />
                <div className="xs z-20 mb-2">
                  You can click here to go back to a previous question.
                </div>
                <div className="xs text-black-light">Okay, got it</div>
              </div>
              <div className="w-2/4">
                <h1
                  className="text-3xxl text-gray-heirs w-full mt-22 mb-10"
                  onClick={() => previousStep()}
                >
                  Select Type of Cover
                </h1>
                <div className="mb-5">
                  <div className="h-nav bg-white border rounded-12 border-gray-lightest w-full opt">
                    <div className="flex items-center h-full ml-5">
                      <svg
                        className="mr-5"
                        width={30}
                        height={22}
                        viewBox="0 0 30 22"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M26.9998 17.1904V20.0476C26.9998 20.3002 26.8944 20.5424 26.7069 20.721C26.5193 20.8996 26.265 21 25.9998 21H22.9998C22.7345 21 22.4802 20.8996 22.2926 20.721C22.1051 20.5424 21.9998 20.3002 21.9998 20.0476V17.1904H26.9998Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M7.99976 17.1904V20.0476C7.99976 20.3002 7.8944 20.5424 7.70686 20.721C7.51933 20.8996 7.26497 21 6.99976 21H3.99976C3.73454 21 3.48019 20.8996 3.29265 20.721C3.10511 20.5424 2.99976 20.3002 2.99976 20.0476V17.1904H7.99976Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M26.9998 9.57143L23.2637 1.56558C23.1851 1.39724 23.057 1.2542 22.8948 1.15382C22.7326 1.05343 22.5433 1 22.3499 1H7.64963C7.45619 1 7.26691 1.05343 7.10472 1.15382C6.94252 1.2542 6.81438 1.39724 6.73582 1.56558L2.99976 9.57143H26.9998Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M1 9.57153H29"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                        <path
                          d="M26.9998 17.1904V20.0476C26.9998 20.3002 26.8944 20.5424 26.7069 20.721C26.5193 20.8996 26.265 21 25.9998 21H22.9998C22.7345 21 22.4802 20.8996 22.2926 20.721C22.1051 20.5424 21.9998 20.3002 21.9998 20.0476V17.1904"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                        <path
                          d="M7.99976 17.1904V20.0476C7.99976 20.3002 7.8944 20.5424 7.70686 20.721C7.51933 20.8996 7.26497 21 6.99976 21H3.99976C3.73454 21 3.48019 20.8996 3.29265 20.721C3.10511 20.5424 2.99976 20.3002 2.99976 20.0476V17.1904"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                        <path
                          d="M6.99976 13.381H8.99976"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                        <path
                          d="M21 13.381H23"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                        <path
                          d="M26.9998 9.57143L23.2637 1.56558C23.1851 1.39724 23.057 1.2542 22.8948 1.15382C22.7326 1.05343 22.5433 1 22.3499 1H7.64963C7.4562 1 7.26691 1.05343 7.10472 1.15382C6.94252 1.2542 6.81438 1.39724 6.73582 1.56558L2.99976 9.57143V17.1905H26.9998V9.57143Z"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                      </svg>
                      <div
                        className="text-xl text-black-dark cursor-pointer"
                        onClick={() => {
                          nextStep();
                          setCarDetails({
                            ...carDetails,
                            motorClass: "Private",
                          });
                        }}
                      >
                        Private
                      </div>
                    </div>
                  </div>
                </div>
                <div className="mb-5">
                  <div className="h-nav bg-white border rounded-12 border-gray-lightest w-full mb-5 opt">
                    <div className="flex items-center h-full ml-5">
                      <svg
                        className="mr-5"
                        width={29}
                        height={22}
                        viewBox="0 0 29 22"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M18.8571 10.4117H0V16.2941C0 16.6061 0.120407 16.9054 0.334734 17.126C0.549061 17.3466 0.83975 17.4706 1.14285 17.4706H2.99999C2.99999 16.5345 3.36121 15.6368 4.00419 14.9749C4.64717 14.313 5.51924 13.9412 6.42855 13.9412C7.33786 13.9412 8.20993 14.313 8.85291 14.9749C9.49589 15.6368 9.85711 16.5345 9.85711 17.4706H17.1428C17.1425 16.8509 17.3008 16.2422 17.6017 15.7055C17.9026 15.1688 18.3355 14.7232 18.8568 14.4135L18.8571 10.4117Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M23.9998 17.4706C23.9996 16.851 23.8411 16.2425 23.5402 15.706C23.2392 15.1695 22.8064 14.724 22.2852 14.4142C21.764 14.1044 21.1729 13.9413 20.5711 13.9411C19.9692 13.941 19.378 14.1039 18.8567 14.4135L18.857 6.88232H27.9998V16.2941C27.9998 16.6061 27.8794 16.9053 27.6651 17.126C27.4507 17.3466 27.16 17.4706 26.8569 17.4706H23.9998Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M28 6.88235H18.8572V1H24.9406C25.169 1 25.3921 1.07045 25.5813 1.20227C25.7704 1.33409 25.9168 1.52123 26.0017 1.73954L28 6.88235Z"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                        <path
                          d="M1 10.4117H18.8571"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                        <path
                          d="M20.5713 21C22.4648 21 23.9998 19.4198 23.9998 17.4706C23.9998 15.5213 22.4648 13.9412 20.5713 13.9412C18.6777 13.9412 17.1427 15.5213 17.1427 17.4706C17.1427 19.4198 18.6777 21 20.5713 21Z"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeMiterlimit={10}
                        />
                        <path
                          d="M6.42881 21C8.32235 21 9.85737 19.4198 9.85737 17.4706C9.85737 15.5213 8.32235 13.9412 6.42881 13.9412C4.53526 13.9412 3.00024 15.5213 3.00024 17.4706C3.00024 19.4198 4.53526 21 6.42881 21Z"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeMiterlimit={10}
                        />
                        <path
                          d="M17.1429 17.4706H9.85718"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                        <path
                          d="M18.8572 14.4139V6.88232H28V16.2941C28 16.6061 27.8796 16.9053 27.6653 17.126C27.4509 17.3466 27.1603 17.4706 26.8572 17.4706H24"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                      </svg>
                      <div
                        className="text-xl text-black-dark cursor-pointer"
                        onClick={() => {
                          nextStep();
                          setCarDetails({
                            ...carDetails,
                            motorClass: "Commercial",
                          });
                        }}
                      >
                        Commercial
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : step == 3 ? (
            <div class="ml-20 w-full">
              <div class="mb-20">
                <h1 class="text-gray-lightest text-xl w-full">
                  Choose Vehicle Class
                </h1>
                <h1 class="text-gray-lightest text-xl font-bold w-full">
                  {carDetails.motorClass}
                </h1>
              </div>

              <div class="w-2/4">
                <h1 class="text-3xxl text-gray-heirs w-full mt-22 mb-10">
                  What type of car do you Drive?
                </h1>
                <div
                  class="mb-5"
                  onClick={() => {
                    nextStep();
                    setCarDetails({
                      ...carDetails,
                      motorType: { type: "Sedan", price: "N30,000" },
                    });
                  }}
                >
                  <div class="h-nav bg-white border rounded-12 border-gray-lightest w-4/5 opt">
                    <div class="flex items-center h-full ml-5">
                      <svg
                        class="mr-5"
                        width="30"
                        height="22"
                        viewBox="0 0 30 22"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M26.9998 17.1904V20.0476C26.9998 20.3002 26.8944 20.5424 26.7069 20.721C26.5193 20.8996 26.265 21 25.9998 21H22.9998C22.7345 21 22.4802 20.8996 22.2926 20.721C22.1051 20.5424 21.9998 20.3002 21.9998 20.0476V17.1904H26.9998Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M7.99976 17.1904V20.0476C7.99976 20.3002 7.8944 20.5424 7.70686 20.721C7.51933 20.8996 7.26497 21 6.99976 21H3.99976C3.73454 21 3.48019 20.8996 3.29265 20.721C3.10511 20.5424 2.99976 20.3002 2.99976 20.0476V17.1904H7.99976Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M26.9998 9.57143L23.2637 1.56558C23.1851 1.39724 23.057 1.2542 22.8948 1.15382C22.7326 1.05343 22.5433 1 22.3499 1H7.64963C7.45619 1 7.26691 1.05343 7.10472 1.15382C6.94252 1.2542 6.81438 1.39724 6.73582 1.56558L2.99976 9.57143H26.9998Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M1 9.57153H29"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                        <path
                          d="M26.9998 17.1904V20.0476C26.9998 20.3002 26.8944 20.5424 26.7069 20.721C26.5193 20.8996 26.265 21 25.9998 21H22.9998C22.7345 21 22.4802 20.8996 22.2926 20.721C22.1051 20.5424 21.9998 20.3002 21.9998 20.0476V17.1904"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                        <path
                          d="M7.99976 17.1904V20.0476C7.99976 20.3002 7.8944 20.5424 7.70686 20.721C7.51933 20.8996 7.26497 21 6.99976 21H3.99976C3.73454 21 3.48019 20.8996 3.29265 20.721C3.10511 20.5424 2.99976 20.3002 2.99976 20.0476V17.1904"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                        <path
                          d="M6.99976 13.381H8.99976"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                        <path
                          d="M21 13.381H23"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                        <path
                          d="M26.9998 9.57143L23.2637 1.56558C23.1851 1.39724 23.057 1.2542 22.8948 1.15382C22.7326 1.05343 22.5433 1 22.3499 1H7.64963C7.4562 1 7.26691 1.05343 7.10472 1.15382C6.94252 1.2542 6.81438 1.39724 6.73582 1.56558L2.99976 9.57143V17.1905H26.9998V9.57143Z"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                      </svg>
                      <div class="flex w-full justify-between pr-5">
                        <div class="text-xl text-black-dark">Sedan</div>
                        <div class="text-xl text-black-dark">N30,000</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  class="mb-5"
                  onClick={() => {
                    nextStep();
                    setCarDetails({
                      ...carDetails,
                      motorType: { type: "SUV", price: "N40,000" },
                    });
                  }}
                >
                  <div class="h-nav bg-white border rounded-12 border-gray-lightest w-4/5 opt">
                    <div class="flex items-center h-full ml-5">
                      <svg
                        class="mr-5"
                        width="29"
                        height="22"
                        viewBox="0 0 29 22"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M18.8571 10.4117H0V16.2941C0 16.6061 0.120407 16.9054 0.334734 17.126C0.549061 17.3466 0.83975 17.4706 1.14285 17.4706H2.99999C2.99999 16.5345 3.36121 15.6368 4.00419 14.9749C4.64717 14.313 5.51924 13.9412 6.42855 13.9412C7.33786 13.9412 8.20993 14.313 8.85291 14.9749C9.49589 15.6368 9.85711 16.5345 9.85711 17.4706H17.1428C17.1425 16.8509 17.3008 16.2422 17.6017 15.7055C17.9026 15.1688 18.3355 14.7232 18.8568 14.4135L18.8571 10.4117Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M23.9998 17.4706C23.9996 16.851 23.8411 16.2425 23.5402 15.706C23.2392 15.1695 22.8064 14.724 22.2852 14.4142C21.764 14.1044 21.1729 13.9413 20.5711 13.9411C19.9692 13.941 19.378 14.1039 18.8567 14.4135L18.857 6.88232H27.9998V16.2941C27.9998 16.6061 27.8794 16.9053 27.6651 17.126C27.4507 17.3466 27.16 17.4706 26.8569 17.4706H23.9998Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M28 6.88235H18.8572V1H24.9406C25.169 1 25.3921 1.07045 25.5813 1.20227C25.7704 1.33409 25.9168 1.52123 26.0017 1.73954L28 6.88235Z"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                        <path
                          d="M1 10.4117H18.8571"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                        <path
                          d="M20.5713 21C22.4648 21 23.9998 19.4198 23.9998 17.4706C23.9998 15.5213 22.4648 13.9412 20.5713 13.9412C18.6777 13.9412 17.1427 15.5213 17.1427 17.4706C17.1427 19.4198 18.6777 21 20.5713 21Z"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-miterlimit="10"
                        />
                        <path
                          d="M6.42881 21C8.32235 21 9.85737 19.4198 9.85737 17.4706C9.85737 15.5213 8.32235 13.9412 6.42881 13.9412C4.53526 13.9412 3.00024 15.5213 3.00024 17.4706C3.00024 19.4198 4.53526 21 6.42881 21Z"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-miterlimit="10"
                        />
                        <path
                          d="M17.1429 17.4706H9.85718"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                        <path
                          d="M18.8572 14.4139V6.88232H28V16.2941C28 16.6061 27.8796 16.9053 27.6653 17.126C27.4509 17.3466 27.1603 17.4706 26.8572 17.4706H24"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                      </svg>
                      <div class="flex w-full justify-between pr-5">
                        <div class="text-xl text-black-dark">SUV</div>
                        <div class="text-xl text-black-dark">N40,000</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  class="mb-5"
                  onClick={() => {
                    nextStep();
                    setCarDetails({
                      ...carDetails,
                      motorType: { type: "Light Truck", price: "N50,000" },
                    });
                  }}
                >
                  <div class="h-nav bg-white border rounded-12 border-gray-lightest w-4/5 opt">
                    <div class="flex items-center h-full ml-5">
                      <svg
                        class="mr-5"
                        width="29"
                        height="22"
                        viewBox="0 0 29 22"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M18.8571 10.4117H0V16.2941C0 16.6061 0.120407 16.9054 0.334734 17.126C0.549061 17.3466 0.83975 17.4706 1.14285 17.4706H2.99999C2.99999 16.5345 3.36121 15.6368 4.00419 14.9749C4.64717 14.313 5.51924 13.9412 6.42855 13.9412C7.33786 13.9412 8.20993 14.313 8.85291 14.9749C9.49589 15.6368 9.85711 16.5345 9.85711 17.4706H17.1428C17.1425 16.8509 17.3008 16.2422 17.6017 15.7055C17.9026 15.1688 18.3355 14.7232 18.8568 14.4135L18.8571 10.4117Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M23.9998 17.4706C23.9996 16.851 23.8411 16.2425 23.5402 15.706C23.2392 15.1695 22.8064 14.724 22.2852 14.4142C21.764 14.1044 21.1729 13.9413 20.5711 13.9411C19.9692 13.941 19.378 14.1039 18.8567 14.4135L18.857 6.88232H27.9998V16.2941C27.9998 16.6061 27.8794 16.9053 27.6651 17.126C27.4507 17.3466 27.16 17.4706 26.8569 17.4706H23.9998Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M28 6.88235H18.8572V1H24.9406C25.169 1 25.3921 1.07045 25.5813 1.20227C25.7704 1.33409 25.9168 1.52123 26.0017 1.73954L28 6.88235Z"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                        <path
                          d="M1 10.4117H18.8571"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                        <path
                          d="M20.5713 21C22.4648 21 23.9998 19.4198 23.9998 17.4706C23.9998 15.5213 22.4648 13.9412 20.5713 13.9412C18.6777 13.9412 17.1427 15.5213 17.1427 17.4706C17.1427 19.4198 18.6777 21 20.5713 21Z"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-miterlimit="10"
                        />
                        <path
                          d="M6.42881 21C8.32235 21 9.85737 19.4198 9.85737 17.4706C9.85737 15.5213 8.32235 13.9412 6.42881 13.9412C4.53526 13.9412 3.00024 15.5213 3.00024 17.4706C3.00024 19.4198 4.53526 21 6.42881 21Z"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-miterlimit="10"
                        />
                        <path
                          d="M17.1429 17.4706H9.85718"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                        <path
                          d="M18.8572 14.4139V6.88232H28V16.2941C28 16.6061 27.8796 16.9053 27.6653 17.126C27.4509 17.3466 27.1603 17.4706 26.8572 17.4706H24"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                      </svg>
                      <div class="flex w-full justify-between pr-5">
                        <div class="text-xl text-black-dark">Light Truck</div>
                        <div class="text-xl text-black-dark">N50,000</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  class="mb-5"
                  onClick={() => {
                    nextStep();
                    setCarDetails({
                      ...carDetails,
                      motorType: { type: "Heavy Truck", price: "N60,000" },
                    });
                  }}
                >
                  <div class="h-nav bg-white border rounded-12 border-gray-lightest w-4/5 opt">
                    <div class="flex items-center h-full ml-5">
                      <svg
                        class="mr-5"
                        width="29"
                        height="22"
                        viewBox="0 0 29 22"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M18.8571 10.4117H0V16.2941C0 16.6061 0.120407 16.9054 0.334734 17.126C0.549061 17.3466 0.83975 17.4706 1.14285 17.4706H2.99999C2.99999 16.5345 3.36121 15.6368 4.00419 14.9749C4.64717 14.313 5.51924 13.9412 6.42855 13.9412C7.33786 13.9412 8.20993 14.313 8.85291 14.9749C9.49589 15.6368 9.85711 16.5345 9.85711 17.4706H17.1428C17.1425 16.8509 17.3008 16.2422 17.6017 15.7055C17.9026 15.1688 18.3355 14.7232 18.8568 14.4135L18.8571 10.4117Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M23.9998 17.4706C23.9996 16.851 23.8411 16.2425 23.5402 15.706C23.2392 15.1695 22.8064 14.724 22.2852 14.4142C21.764 14.1044 21.1729 13.9413 20.5711 13.9411C19.9692 13.941 19.378 14.1039 18.8567 14.4135L18.857 6.88232H27.9998V16.2941C27.9998 16.6061 27.8794 16.9053 27.6651 17.126C27.4507 17.3466 27.16 17.4706 26.8569 17.4706H23.9998Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M28 6.88235H18.8572V1H24.9406C25.169 1 25.3921 1.07045 25.5813 1.20227C25.7704 1.33409 25.9168 1.52123 26.0017 1.73954L28 6.88235Z"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                        <path
                          d="M1 10.4117H18.8571"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                        <path
                          d="M20.5713 21C22.4648 21 23.9998 19.4198 23.9998 17.4706C23.9998 15.5213 22.4648 13.9412 20.5713 13.9412C18.6777 13.9412 17.1427 15.5213 17.1427 17.4706C17.1427 19.4198 18.6777 21 20.5713 21Z"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-miterlimit="10"
                        />
                        <path
                          d="M6.42881 21C8.32235 21 9.85737 19.4198 9.85737 17.4706C9.85737 15.5213 8.32235 13.9412 6.42881 13.9412C4.53526 13.9412 3.00024 15.5213 3.00024 17.4706C3.00024 19.4198 4.53526 21 6.42881 21Z"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-miterlimit="10"
                        />
                        <path
                          d="M17.1429 17.4706H9.85718"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                        <path
                          d="M18.8572 14.4139V6.88232H28V16.2941C28 16.6061 27.8796 16.9053 27.6653 17.126C27.4509 17.3466 27.1603 17.4706 26.8572 17.4706H24"
                          stroke="#5E5E5E"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                      </svg>
                      <div class="flex w-full justify-between pr-5">
                        <div class="text-xl text-black-dark">Heavy Truck</div>
                        <div class="text-xl text-black-dark">N60,000</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : step == 4 ? (
            <div className="ml-20 w-full">
              <div className="mb-20" onClick={previousStep}>
                <h1 className="text-gray-lightest text-xl w-full">
                  What type of car do you drive?
                </h1>
                <h1 className="text-gray-lightest text-xl font-bold w-full">
                  {carDetails.motorType.type}
                </h1>
              </div>
              <div className="w-3/4">
                <h1 className="text-3xxl text-black-dark w-full mt-22 mb-10">
                  What is the value of your car?
                </h1>
                <form onSubmit={nextFormField}>
                  <input
                    type="text"
                    name="value"
                    className="border-b border-black-heirs w-3/5 outline-none"
                    placeholder="Enter Amount"
                    autocomplete="off"
                    value={carDetails.value}
                    onChange={getCarDetails}
                  />
                  <div className="flex items-center mt-7">
                    {carDetails.value && (
                      <>
                        <div
                          className="border border-heirsred rounded-md px-5 py-2 flex items-center cursor-pointer"
                          onClick={nextStep}
                        >
                          Continue
                        </div>

                        <div className="xs text-gray-dark ml-3 flex items-center">
                          or Press <span className="font-bold">Enter</span>
                          <img
                            src="/assets/Group.png"
                            alt="enter"
                            className="h-4 w-auto ml-2"
                          />
                        </div>
                      </>
                    )}
                  </div>
                </form>
              </div>
            </div>
          ) : step == 5 ? (
            <div class="ml-20 w-full">
              <div class="mb-20">
                <h1 class="text-gray-lightest text-xl w-full">
                  What is the value of your car?
                </h1>
                <h1 class="text-gray-lightest text-xl font-bold w-full">
                  {carDetails.value}
                </h1>
              </div>

              <div class="w-full">
                <h1 class="text-22 text-black-heirs w-full mt-22 mb-10">
                  Your Quoted Premium is:
                </h1>
                <div class="grid grid-cols-3 gap-8">
                  <div
                    class="h-80 w-60 border rounded-md border-gray-lightest bg-white mb-9 py-6 flex flex-col items-center cursor-pointer"
                    onClick={() => {
                      setCarDetails({
                        ...carDetails,
                        quotedPremium: "3rd Party Plus",
                      });
                      nextStep();
                    }}
                  >
                    <div class="text-xl font-medium text-gray-heirs mb-6">
                      3rd Party Plus
                    </div>
                    <div class="flex flex-col h-52 justify-center items-center">
                      <div class="text-base text-aa mb-5">3rd Party Cover</div>
                      <div class="text-base text-aa mb-5">Car Theft</div>
                      <div class="text-base text-aa">Car Fire</div>
                    </div>
                    <div class="text-3xxl text-heirsred">₦20,000</div>
                  </div>
                  <div
                    class="h-80 w-60 border rounded-md border-gray-lightest bg-white mb-9 py-6 flex flex-col items-center cursor-pointer"
                    onClick={() => {
                      setCarDetails({
                        ...carDetails,
                        quotedPremium: "Comprehensive",
                      });
                      nextStep();
                    }}
                  >
                    <div class="text-xl font-medium text-gray-heirs mb-6">
                      Comprehensive
                    </div>
                    <div class="flex flex-col h-52 justify-center items-center">
                      <div class="text-base text-aa mb-5">Owner Car Damage</div>
                      <div class="text-base text-aa mb-5">3rd Party Cover</div>
                      <div class="text-base text-aa mb-5">Car Theft</div>
                      <div class="text-base text-aa">Car Fire</div>
                    </div>
                    <div class="text-3xxl text-heirsred">₦30,000</div>
                  </div>
                </div>
              </div>
            </div>
          ) : step == 6 ? (
            <div class="ml-20">
              <div class="mb-10">
              <h1 class="text-gray-lightest text-xl">
                What type of Insurance would you like to buy?
              </h1>
              <h1 class="text-gray-lightest text-xl font-bold">{carDetails.quotedPremium}</h1>
              </div>
              <div class="w-full">
                <h1 class="text-3xxl text-black-dark w-full mt-22 mb-10">
                  Verify Property
                </h1>
                <div class="mb-5">
                  <div class="h-nav bg-white border rounded-12 border-gray-lightest w-full">
                    <div class="flex items-center h-full ml-5">
                      <div class="text-xl text-black-dark">
                        Start Video Verification
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : null}
        </div>
      </div>
    </>
  );
}

export default MotorInssuranceFlow;
