import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";

function RegistrationFlow(props) {

  const history = useHistory()
  const [step, setStep] = useState(1);
  const [addressInput, setAddressInput] = useState(false);
  const [userDetails, setUserDetails] = useState({
    firstName: "",
    lastName: "",
    dateOfBirth: "",
    gender: "",
    phone: "",
    occupation: "",
    photoUrl: "",
    address: "",
    idCardImgUrl: "",
    utilityImgUrl: "",
  });
  const [coverType, setCoverType] = useState("");
  const [stepList, setStepList] = useState([1]);

  const nextStep = () => {
    setStep(step + 1);
    setStepList([...stepList, stepList[stepList.length - 1] + 1]);
  };

  const previousStep = () => {
    setStep(step - 1);
    const newStepList = stepList.filter((step) => step !== stepList.length);
    setStepList(newStepList);
  };

  const getUserDetails = (e) => {
    let { name, value } = e.target;
    setUserDetails({ ...userDetails, [name]: value });
    console.log(userDetails);
  };

  const geoLocationFunction = () => {};

  const nextFormField= (e) => {
    e.preventDefault()
    nextStep()
  }

  return (
    <>
      <div className="h-24 w-full bg-pink-light flex items-center">
        <h6 className="text-heirsred h-5 m-0 pl-20">Buy Insurance</h6>
      </div>
      <div className="ml-20 mt-16">
        <div className="flex">
          <div>
            <div
              className="h-14 w-14 rounded-full bg-white shadow-sm flex items-center justify-center cursor-pointer"
              onClick={step !== 1 ? previousStep : ""}
            >
              <img src="/assets/Back Arrow.png" alt />
            </div>
            <div className="h-24 border-l w-0 ml-7 my-2 border-black-dim" />
            <ul className="ml-5">
              {stepList.map((sl) => (
                <li
                  className={`rounded-full border ${
                    step == sl
                      ? "border-heirsred"
                      : "border-black-dim bg-pink-light"
                  } text-heirsred text-xsm h-4 w-4 flex items-center justify-center mb-4 cursor-pointer`}
                  onClick={() => {
                    setStepList(stepList.splice(0, sl));
                    setStep(sl);
                  }}
                >
                  {sl}
                </li>
              ))}
            </ul>
          </div>
          {step == 1 ? (
            <div className="ml-20 w-full relative ">
              <div className="mb-20">
                <h1 className="text-gray-lightest text-xl w-full">
                  Select Type of Cover
                </h1>
                <h1 className="text-gray-lightest text-xl font-bold w-full">
                  3rd Party
                </h1>
              </div>
              <div className="absolute w-60 h-50 rounded-md bg-black-dim px-5 py-2 tools top-0 left-1/4">
                <div className="tool-tip z-0" />
                <div className="xs z-20 mb-2">
                  You can click here to go back to a previous question.
                </div>
                <div className="xs text-black-light">Okay, got it</div>
              </div>
              <div className="w-2/4">
                <h1
                  className="text-3xxl text-gray-heirs w-full mt-22 mb-10"
                  onClick={() => previousStep()}
                >
                  Select Type of Cover
                </h1>
                <div className="mb-5">
                  <div className="h-nav bg-white border rounded-12 border-gray-lightest w-full opt">
                    <div className="flex items-center h-full ml-5">
                      <svg
                        className="mr-5"
                        width={30}
                        height={22}
                        viewBox="0 0 30 22"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M26.9998 17.1904V20.0476C26.9998 20.3002 26.8944 20.5424 26.7069 20.721C26.5193 20.8996 26.265 21 25.9998 21H22.9998C22.7345 21 22.4802 20.8996 22.2926 20.721C22.1051 20.5424 21.9998 20.3002 21.9998 20.0476V17.1904H26.9998Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M7.99976 17.1904V20.0476C7.99976 20.3002 7.8944 20.5424 7.70686 20.721C7.51933 20.8996 7.26497 21 6.99976 21H3.99976C3.73454 21 3.48019 20.8996 3.29265 20.721C3.10511 20.5424 2.99976 20.3002 2.99976 20.0476V17.1904H7.99976Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M26.9998 9.57143L23.2637 1.56558C23.1851 1.39724 23.057 1.2542 22.8948 1.15382C22.7326 1.05343 22.5433 1 22.3499 1H7.64963C7.45619 1 7.26691 1.05343 7.10472 1.15382C6.94252 1.2542 6.81438 1.39724 6.73582 1.56558L2.99976 9.57143H26.9998Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M1 9.57153H29"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                        <path
                          d="M26.9998 17.1904V20.0476C26.9998 20.3002 26.8944 20.5424 26.7069 20.721C26.5193 20.8996 26.265 21 25.9998 21H22.9998C22.7345 21 22.4802 20.8996 22.2926 20.721C22.1051 20.5424 21.9998 20.3002 21.9998 20.0476V17.1904"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                        <path
                          d="M7.99976 17.1904V20.0476C7.99976 20.3002 7.8944 20.5424 7.70686 20.721C7.51933 20.8996 7.26497 21 6.99976 21H3.99976C3.73454 21 3.48019 20.8996 3.29265 20.721C3.10511 20.5424 2.99976 20.3002 2.99976 20.0476V17.1904"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                        <path
                          d="M6.99976 13.381H8.99976"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                        <path
                          d="M21 13.381H23"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                        <path
                          d="M26.9998 9.57143L23.2637 1.56558C23.1851 1.39724 23.057 1.2542 22.8948 1.15382C22.7326 1.05343 22.5433 1 22.3499 1H7.64963C7.4562 1 7.26691 1.05343 7.10472 1.15382C6.94252 1.2542 6.81438 1.39724 6.73582 1.56558L2.99976 9.57143V17.1905H26.9998V9.57143Z"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                      </svg>
                      <div
                        className="text-xl text-black-dark cursor-pointer"
                        onClick={() => {
                          nextStep();
                          setCoverType("3rd Party");
                        }}
                      >
                        3rd Party
                      </div>
                    </div>
                  </div>
                </div>
                <div className="mb-5">
                  <div className="h-nav bg-white border rounded-12 border-gray-lightest w-full mb-5 opt">
                    <div className="flex items-center h-full ml-5">
                      <svg
                        className="mr-5"
                        width={29}
                        height={22}
                        viewBox="0 0 29 22"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M18.8571 10.4117H0V16.2941C0 16.6061 0.120407 16.9054 0.334734 17.126C0.549061 17.3466 0.83975 17.4706 1.14285 17.4706H2.99999C2.99999 16.5345 3.36121 15.6368 4.00419 14.9749C4.64717 14.313 5.51924 13.9412 6.42855 13.9412C7.33786 13.9412 8.20993 14.313 8.85291 14.9749C9.49589 15.6368 9.85711 16.5345 9.85711 17.4706H17.1428C17.1425 16.8509 17.3008 16.2422 17.6017 15.7055C17.9026 15.1688 18.3355 14.7232 18.8568 14.4135L18.8571 10.4117Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M23.9998 17.4706C23.9996 16.851 23.8411 16.2425 23.5402 15.706C23.2392 15.1695 22.8064 14.724 22.2852 14.4142C21.764 14.1044 21.1729 13.9413 20.5711 13.9411C19.9692 13.941 19.378 14.1039 18.8567 14.4135L18.857 6.88232H27.9998V16.2941C27.9998 16.6061 27.8794 16.9053 27.6651 17.126C27.4507 17.3466 27.16 17.4706 26.8569 17.4706H23.9998Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M28 6.88235H18.8572V1H24.9406C25.169 1 25.3921 1.07045 25.5813 1.20227C25.7704 1.33409 25.9168 1.52123 26.0017 1.73954L28 6.88235Z"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                        <path
                          d="M1 10.4117H18.8571"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                        <path
                          d="M20.5713 21C22.4648 21 23.9998 19.4198 23.9998 17.4706C23.9998 15.5213 22.4648 13.9412 20.5713 13.9412C18.6777 13.9412 17.1427 15.5213 17.1427 17.4706C17.1427 19.4198 18.6777 21 20.5713 21Z"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeMiterlimit={10}
                        />
                        <path
                          d="M6.42881 21C8.32235 21 9.85737 19.4198 9.85737 17.4706C9.85737 15.5213 8.32235 13.9412 6.42881 13.9412C4.53526 13.9412 3.00024 15.5213 3.00024 17.4706C3.00024 19.4198 4.53526 21 6.42881 21Z"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeMiterlimit={10}
                        />
                        <path
                          d="M17.1429 17.4706H9.85718"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                        <path
                          d="M18.8572 14.4139V6.88232H28V16.2941C28 16.6061 27.8796 16.9053 27.6653 17.126C27.4509 17.3466 27.1603 17.4706 26.8572 17.4706H24"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                      </svg>
                      <div
                        className="text-xl text-black-dark cursor-pointer"
                        onClick={() => {
                          nextStep();
                          setCoverType("3rd Party Plus");
                        }}
                      >
                        3rd Party Plus
                      </div>
                    </div>
                  </div>
                </div>
                <div className="mb-5">
                  <div className="h-nav bg-white border rounded-12 border-gray-lightest w-full mb-5 opt">
                    <div className="flex items-center h-full ml-5">
                      <svg
                        className="mr-5"
                        width={29}
                        height={22}
                        viewBox="0 0 29 22"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M18.8571 10.4117H0V16.2941C0 16.6061 0.120407 16.9054 0.334734 17.126C0.549061 17.3466 0.83975 17.4706 1.14285 17.4706H2.99999C2.99999 16.5345 3.36121 15.6368 4.00419 14.9749C4.64717 14.313 5.51924 13.9412 6.42855 13.9412C7.33786 13.9412 8.20993 14.313 8.85291 14.9749C9.49589 15.6368 9.85711 16.5345 9.85711 17.4706H17.1428C17.1425 16.8509 17.3008 16.2422 17.6017 15.7055C17.9026 15.1688 18.3355 14.7232 18.8568 14.4135L18.8571 10.4117Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M23.9998 17.4706C23.9996 16.851 23.8411 16.2425 23.5402 15.706C23.2392 15.1695 22.8064 14.724 22.2852 14.4142C21.764 14.1044 21.1729 13.9413 20.5711 13.9411C19.9692 13.941 19.378 14.1039 18.8567 14.4135L18.857 6.88232H27.9998V16.2941C27.9998 16.6061 27.8794 16.9053 27.6651 17.126C27.4507 17.3466 27.16 17.4706 26.8569 17.4706H23.9998Z"
                          fill="#D9D9D9"
                        />
                        <path
                          d="M28 6.88235H18.8572V1H24.9406C25.169 1 25.3921 1.07045 25.5813 1.20227C25.7704 1.33409 25.9168 1.52123 26.0017 1.73954L28 6.88235Z"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                        <path
                          d="M1 10.4117H18.8571"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                        <path
                          d="M20.5713 21C22.4648 21 23.9998 19.4198 23.9998 17.4706C23.9998 15.5213 22.4648 13.9412 20.5713 13.9412C18.6777 13.9412 17.1427 15.5213 17.1427 17.4706C17.1427 19.4198 18.6777 21 20.5713 21Z"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeMiterlimit={10}
                        />
                        <path
                          d="M6.42881 21C8.32235 21 9.85737 19.4198 9.85737 17.4706C9.85737 15.5213 8.32235 13.9412 6.42881 13.9412C4.53526 13.9412 3.00024 15.5213 3.00024 17.4706C3.00024 19.4198 4.53526 21 6.42881 21Z"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeMiterlimit={10}
                        />
                        <path
                          d="M17.1429 17.4706H9.85718"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                        <path
                          d="M18.8572 14.4139V6.88232H28V16.2941C28 16.6061 27.8796 16.9053 27.6653 17.126C27.4509 17.3466 27.1603 17.4706 26.8572 17.4706H24"
                          stroke="#5E5E5E"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                      </svg>
                      <div
                        className="text-xl text-black-dark cursor-pointer"
                        onClick={() => {
                          nextStep();
                          setCoverType("Comprehensive");
                        }}
                      >
                        Comprehensive
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : step == 2 ? (
            <div className="ml-20 w-full">
              <div className="mb-20" onClick={previousStep}>
                <h1 className="text-gray-lightest text-xl w-full">
                  Cover Type
                </h1>
                <h1 className="text-gray-lightest text-xl font-bold w-full">
                  {coverType}
                </h1>
              </div>
              <div className="w-3/4">
                <h1 className="text-3xxl text-black-dark w-full mt-22 mb-10">
                  What is your <span className="font-bold">firstname</span>?
                </h1>
                <form onSubmit={nextFormField}>
                  <input
                    type="text"
                    name="firstName"
                    className="border-b border-black-heirs w-3/5 outline-none"
                    placeholder="Enter Firstname..."
                    value={userDetails.firstName}
                    onChange={getUserDetails}
                  />
                  <div className="flex items-center mt-7">
                    {userDetails.firstName && (
                      <>
                        <div
                          className="border border-heirsred rounded-md px-5 py-2 flex items-center cursor-pointer"
                          onClick={nextStep}
                        >
                          Continue
                        </div>

                        <div className="xs text-gray-dark ml-3 flex items-center">
                          or Press <span className="font-bold">Enter</span>
                          <img
                            src="/assets/Group.png"
                            alt="enter"
                            className="h-4 w-auto ml-2"
                          />
                        </div>
                      </>
                    )}
                  </div>
                </form>
              </div>
            </div>
          ) : step == 3 ? (
            <div className="ml-20 w-full">
              <div className="mb-20" onClick={previousStep}>
                <h1 className="text-gray-lightest text-xl w-full">
                  Whats your Firstname
                </h1>
                <h1 className="text-gray-lightest text-xl font-bold w-full">
                  {userDetails.firstName}
                </h1>
              </div>
              <div className="w-3/4">
                <h1 className="text-3xxl text-black-dark w-full mt-22 mb-10">
                  What is your <span className="font-bold">lastname</span>?
                </h1>
                <form onSubmit={nextFormField}>
                  <input
                    type="text"
                    name="lastName"
                    className="border-b border-black-heirs w-3/5 outline-none"
                    placeholder="Enter Lastname..."
                    autocomplete="off"
                    value={userDetails.lastName}
                    onChange={getUserDetails}
                  />
                  <div className="flex items-center mt-7">
                    {userDetails.lastName && (
                      <>
                        <div
                          className="border border-heirsred rounded-md px-5 py-2 flex items-center cursor-pointer"
                          onClick={nextStep}
                        >
                          Continue
                        </div>

                        <div className="xs text-gray-dark ml-3 flex items-center">
                          or Press <span className="font-bold">Enter</span>
                          <img
                            src="/assets/Group.png"
                            alt="enter"
                            className="h-4 w-auto ml-2"
                          />
                        </div>
                      </>
                    )}
                  </div>
                </form>
              </div>
            </div>
          ) : step == 4 ? (
            <div className="ml-20 w-full">
              <div className="mb-20" onClick={previousStep}>
                <h1 className="text-gray-lightest text-xl w-full">
                  What's your Surname
                </h1>
                <h1 className="text-gray-lightest text-xl font-bold w-full">
                  {userDetails.lastName}
                </h1>
              </div>
              <div className="w-3/4">
                <h1 className="text-3xxl text-black-dark w-full mt-22 mb-10">
                  What is your <span className="font-bold">Phone Number</span>?
                </h1>
                <form onSubmit={nextFormField}>
                  <input
                    type="text"
                    name="phone"
                    className="border-b border-black-heirs w-3/5 outline-none"
                    placeholder="Enter Phone number..."
                    value={userDetails.phone}
                    onChange={getUserDetails}
                  />
                  <div className="flex items-center mt-7">
                    {userDetails.phone && (
                      <>
                        <div
                          className="border border-heirsred rounded-md px-5 py-2 flex items-center cursor-pointer"
                          onClick={nextStep}
                        >
                          Continue
                        </div>

                        <div className="xs text-gray-dark ml-3 flex items-center">
                          or Press <span className="font-bold">Enter</span>
                          <img
                            src="/assets/Group.png"
                            alt="enter"
                            className="h-4 w-auto ml-2"
                          />
                        </div>
                      </>
                    )}
                  </div>
                </form>
              </div>
            </div>
          ) : step == 5 ? (
            <div className="ml-20 w-full">
              <div className="mb-20" onClick={previousStep}>
                <h1 className="text-gray-lightest text-xl w-full">
                  What is your Phone Number?
                </h1>
                <h1 className="text-gray-lightest text-xl font-bold w-full">
                  {userDetails.phone}
                </h1>
              </div>
              <div className="w-3/4">
                <h1 className="text-3xxl text-black-dark w-full mt-22 mb-10">
                  What is your <span className="font-bold">Date Of Birth?</span>
                  ?
                </h1>
                <form onSubmit={nextFormField}>
                  <input
                    type="date"
                    name="dateOfBirth"
                    className="border-b border-black-heirs w-3/5 outline-none"
                    placeholder="DD/MM/YYYY"
                    value={userDetails.dateOfBirth}
                    onChange={getUserDetails}
                  />
                  <div className="flex items-center mt-7">
                    {userDetails.dateOfBirth && (
                      <>
                        <div
                          className="border border-heirsred rounded-md px-5 py-2 flex items-center cursor-pointer"
                          onClick={nextStep}
                        >
                          Continue
                        </div>

                        <div className="xs text-gray-dark ml-3 flex items-center">
                          or Press <span className="font-bold">Enter</span>
                          <img
                            src="/assets/Group.png"
                            alt="enter"
                            className="h-4 w-auto ml-2"
                          />
                        </div>
                      </>
                    )}
                  </div>
                </form>
              </div>
            </div>
          ) : step == 6 ? (
            <div className="ml-20 w-full">
              <div className="mb-20" onClick={previousStep}>
                <h1 className="text-gray-lightest text-xl w-full">
                  What is your date of birth?
                </h1>
                <h1 className="text-gray-lightest text-xl font-bold w-full">
                  {userDetails.dateOfBirth}
                </h1>
              </div>

              <div className="w-full">
                <h1 className="text-3xxl text-gray-heirs w-full mt-22 mb-10">
                  What’s your <span className="font-bold">Gender?</span>
                </h1>
                <div className="flex">
                  <div
                    className="h-32 w-24 border rounded-md border-gray-lightest bg-white mb-9 py-4 flex flex-col items-center mr-8 cursor-pointer"
                    onClick={() => {
                      setUserDetails({ ...userDetails, gender: "Male" });
                      nextStep();
                    }}
                  >
                    <div className="text-base text-gray-heirs mb-2">Male</div>
                    <img
                      src="/assets/noun_Male_1653644 1.png"
                      alt="male"
                      className="w-auto h-auto"
                    />
                  </div>
                  <div
                    className="h-32 w-24 border rounded-md border-gray-lightest bg-white mb-9 py-4 flex flex-col items-center cursor-pointer"
                    onClick={() => {
                      setUserDetails({ ...userDetails, gender: "Female" });
                      nextStep();
                    }}
                  >
                    <div className="text-base text-gray-heirs mb-2">Female</div>
                    <img
                      src="/assets/noun_Female_1653643 1.png"
                      alt="male"
                      className="w-auto h-auto"
                    />
                  </div>
                </div>
              </div>
            </div>
          ) : step == 7 ? (
            <div className="ml-20 w-full">
              <div className="mb-20" onClick={previousStep}>
                <h1 className="text-gray-lightest text-xl w-full">
                  Whats your Gender?
                </h1>
                <h1 className="text-gray-lightest text-xl font-bold w-full">
                  {userDetails.gender}
                </h1>
              </div>
              <div className="w-3/4">
                <h1 className="text-3xxl text-black-dark w-full mt-22 mb-10">
                  What is your <span className="font-bold">Occupation?</span>?
                </h1>
                <form onSubmit={nextFormField}>
                  <input
                    type="text"
                    name="occupation"
                    className="border-b border-black-heirs w-3/5 outline-none"
                    placeholder="Enter Occupation..."
                    autocomplete="off"
                    value={userDetails.occupation}
                    onChange={getUserDetails}
                  />
                  <div className="flex items-center mt-7">
                    {userDetails.occupation && (
                      <>
                        <div
                          className="border border-heirsred rounded-md px-5 py-2 flex items-center cursor-pointer"
                          onClick={nextStep}
                        >
                          Continue
                        </div>

                        <div className="xs text-gray-dark ml-3 flex items-center">
                          or Press <span className="font-bold">Enter</span>
                          <img
                            src="/assets/Group.png"
                            alt="enter"
                            className="h-4 w-auto ml-2"
                          />
                        </div>
                      </>
                    )}
                  </div>
                </form>
              </div>
            </div>
          ) : step == 8 ? (
            <div className="ml-20 w-full relative">
              {!addressInput ? (
                <>
                  <div className="mb-20">
                    <h1 className="text-gray-lightest text-xl w-full">
                      What is your Occupation?
                    </h1>
                    <h1 className="text-gray-lightest text-xl font-bold w-full">
                      {userDetails.occupation}
                    </h1>
                  </div>
                  <div className="mb-5">
                  <h1 className="text-3xxl text-black-dark mt-22 mb-10 w-1/2">
                      Can we use your current location as your Address?
                    </h1>
                    <div className="h-nav bg-white border rounded-12 border-gray-lightest opt w-65">
                      <div className="flex items-center h-full ml-5">
                        <div className="text-xl text-black-dark" onClick={geoLocationFunction}>Yes</div>
                      </div>
                    </div>
                  </div>

                  <div className="mb-5">
                    <div className="h-nav bg-white border rounded-12 border-gray-lightest mb-5 opt w-65" onClick={() => {
                            setAddressInput(true);
                          }}>
                      <div className="flex items-center h-full ml-5">
                        <div
                          className="text-xl text-black-dark cursor-pointer"
                          
                        >
                          No
                        </div>
                      </div>
                    </div>
                  </div>
                </>
              ) : (
                <div className="ml-20 w-full">
                  <div className="mb-20" onClick={previousStep}>
                    <h1 className="text-gray-lightest text-xl w-full">
                     What is your Occupation?
                    </h1>
                    <h1 className="text-gray-lightest text-xl font-bold w-full">
                      {userDetails.occupation}
                    </h1>
                  </div>
                  <div className="w-3/4">
                    <h1 className="text-3xxl text-black-dark w-full mt-22 mb-10">
                      What is your <span className="font-bold">Address?</span>
                    </h1>
                    <form onSubmit={nextFormField}>
                      <input
                        type="text"
                        name="address"
                        className="border-b border-black-heirs w-3/5 outline-none"
                        placeholder="Enter Address..."
                        value={userDetails.address}
                        onChange={getUserDetails}
                      />
                      <div className="flex items-center mt-7">
                        {userDetails.address && (
                          <>
                            <div
                              className="border border-heirsred rounded-md px-5 py-2 flex items-center cursor-pointer"
                              onClick={nextStep}
                            >
                              Continue
                            </div>

                            <div className="xs text-gray-dark ml-3 flex items-center">
                              or Press <span className="font-bold">Enter</span>
                              <img
                                src="/assets/Group.png"
                                alt="enter"
                                className="h-4 w-auto ml-2"
                              />
                            </div>
                          </>
                        )}
                      </div>
                    </form>
                  </div>
                </div>
              )}
            </div>
          ) : step == 9 ? (
            <div className="ml-20 w-full relative">
              <div className="mb-20">
                    <h1 className="text-gray-lightest text-xl w-full">
                      What is your Address?
                    </h1>
                    <h1 className="text-gray-lightest text-xl font-bold w-full">
                      {userDetails.address}
                    </h1>
                  </div>
                  <div className="mb-5">
                  <h1 className="text-3xxl text-black-dark mt-22 mb-10 w-1/2">
                      Upload Identity ID
                    </h1>
                    <div className="mb-10 h-nav bg-white border rounded-12 border-gray-lightest opt w-65">
                      <div className="flex items-center h-full ml-5" onClick={nextStep}>
                        <div className="text-xl text-black-dark">Capture ID</div>
                      </div>
                      <p className="text-sm text-gray-lightest pt-2">Kindly use a white background when capturing your picture</p>
                    </div>
                  </div>

                  <div className="mb-10 h-nav bg-white border rounded-12 border-gray-lightest opt w-65 pt-4 relative">
                    <label for="id-upload" className="w-full pl-6 rounded-12 border-gray-lightest h-full py-6 cursor-pointer file-label text-xl text-black-dark text-left" onClick={() => {
                            
                          }}>
                            Upload ID
                            <input type="file" id="id-upload" className="invisible-field h-full w-full cursor-pointer absolute top-0 left-0 "/>
                    </label>
                    
                  </div>
                  
            </div>
          ) : step == 10 ? (
            <div className="ml-20 w-full relative">
            <div className="mb-20">
                  <h1 className="text-gray-lightest text-xl w-full">
                    Upload Identity ID
                  </h1>
                  <h1 className="text-gray-lightest text-xl font-bold w-full">
                    {userDetails.idCardImgUrl}
                  </h1>
                </div>
                <div className="mb-5">
                <h1 className="text-3xxl text-black-dark mt-22 mb-10 w-1/2">
                    Upload Utility Bill
                  </h1>
                  <div className="mb-10 h-nav bg-white border rounded-12 border-gray-lightest opt w-65" onClick={history.push('/plans/motor')}>
                    <div className="flex items-center h-full ml-5">
                      <div className="text-xl text-black-dark">Capture Utility Bill</div>
                    </div>
                  </div>
                </div>

                <div className="mb-10 h-nav bg-white border rounded-12 border-gray-lightest opt w-65 pt-4 relative">
                  <label for="utility-upload" className="w-full pl-6 rounded-12 border-gray-lightest h-full py-6 cursor-pointer file-label text-xl text-black-dark text-left" onClick={() => {
                          
                        }}>
                          Upload Utility Bill
                          <input type="file" id="utility-upload" className="invisible-field h-full w-full cursor-pointer absolute top-0 left-0 "/>
                  </label>
                </div>
          </div>
          ) : null}
        </div>
      </div>
    </>
  );
}

export default RegistrationFlow;
