import React, {useState} from "react";
import { Link } from "react-router-dom";


function SelectInssurance(props) {

  const [policyType, setPolicyType] = useState('')

  return (
    <>
      <div class="h-24 w-full bg-pink-light flex items-center">
        <h6 class="text-heirsred h-5 m-0 pl-20">Buy Insurance</h6>
      </div>
      <div className="ml-20 mt-16">
        <div className="flex">
          <div>
            <div className="h-14 w-14 rounded-full bg-white shadow-sm flex items-center justify-center">
              <img src="./assets/Back Arrow.png" alt />
            </div>
            <div className="h-24 border-l w-0 ml-7 my-2 border-black-dim" />
            <ul className="ml-5">
              <li className="rounded-full border border-heirsred text-xsm text-heirsred h-4 w-4 flex items-center justify-center mb-4">
                1
              </li>
              {/* <li className="rounded-full border border-black-dim text-xsm h-4 w-4 flex items-center justify-center text-gray-dark mb-4">
                2
              </li>
              <li className="rounded-full border border-black-dim text-xsm h-4 w-4 flex items-center justify-center text-gray-dark mb-4">
                3
              </li> */}
            </ul>
          </div>
          <div class="ml-20">
          <h1 className="text-3xxl text-black-dark w-65 mb-10">
            What type of Insurance would you like to buy?
          </h1>
          <div className="mb-5">
            <Link to="/kyc/motor">
              <div className="h-nav bg-white border rounded-12 border-gray-lightest w-65">
              <div className="flex items-center h-full ml-5 ">
                <svg
                  className="mr-5"
                  width={30}
                  height={22}
                  viewBox="0 0 30 22"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M26.9998 17.1904V20.0476C26.9998 20.3002 26.8944 20.5424 26.7069 20.721C26.5193 20.8996 26.265 21 25.9998 21H22.9998C22.7345 21 22.4802 20.8996 22.2926 20.721C22.1051 20.5424 21.9998 20.3002 21.9998 20.0476V17.1904H26.9998Z"
                    fill="#D9D9D9"
                  />
                  <path
                    d="M7.99976 17.1904V20.0476C7.99976 20.3002 7.8944 20.5424 7.70686 20.721C7.51933 20.8996 7.26497 21 6.99976 21H3.99976C3.73454 21 3.48019 20.8996 3.29265 20.721C3.10511 20.5424 2.99976 20.3002 2.99976 20.0476V17.1904H7.99976Z"
                    fill="#D9D9D9"
                  />
                  <path
                    d="M26.9998 9.57143L23.2637 1.56558C23.1851 1.39724 23.057 1.2542 22.8948 1.15382C22.7326 1.05343 22.5433 1 22.3499 1H7.64963C7.45619 1 7.26691 1.05343 7.10472 1.15382C6.94252 1.2542 6.81438 1.39724 6.73582 1.56558L2.99976 9.57143H26.9998Z"
                    fill="#D9D9D9"
                  />
                  <path
                    d="M1 9.57153H29"
                    stroke="#5E5E5E"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                  <path
                    d="M26.9998 17.1904V20.0476C26.9998 20.3002 26.8944 20.5424 26.7069 20.721C26.5193 20.8996 26.265 21 25.9998 21H22.9998C22.7345 21 22.4802 20.8996 22.2926 20.721C22.1051 20.5424 21.9998 20.3002 21.9998 20.0476V17.1904"
                    stroke="#5E5E5E"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                  <path
                    d="M7.99976 17.1904V20.0476C7.99976 20.3002 7.8944 20.5424 7.70686 20.721C7.51933 20.8996 7.26497 21 6.99976 21H3.99976C3.73454 21 3.48019 20.8996 3.29265 20.721C3.10511 20.5424 2.99976 20.3002 2.99976 20.0476V17.1904"
                    stroke="#5E5E5E"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                  <path
                    d="M6.99976 13.381H8.99976"
                    stroke="#5E5E5E"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                  <path
                    d="M21 13.381H23"
                    stroke="#5E5E5E"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                  <path
                    d="M26.9998 9.57143L23.2637 1.56558C23.1851 1.39724 23.057 1.2542 22.8948 1.15382C22.7326 1.05343 22.5433 1 22.3499 1H7.64963C7.4562 1 7.26691 1.05343 7.10472 1.15382C6.94252 1.2542 6.81438 1.39724 6.73582 1.56558L2.99976 9.57143V17.1905H26.9998V9.57143Z"
                    stroke="#5E5E5E"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
                
                  <div className="text-xl text-black-dark">Motor</div>
                </div>
                </div>
              </Link>
          </div>
          <div className="mb-5">
            <div className="h-nav bg-white border rounded-12 border-gray-lightest w-65">
              <div className="flex items-center h-full ml-5">
                <svg
                  className="mr-5"
                  width={28}
                  height={28}
                  viewBox="0 0 28 28"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    opacity="0.2"
                    d="M16.6244 22.7492V17.4991C16.6244 17.267 16.5322 17.0444 16.3681 16.8803C16.204 16.7163 15.9814 16.6241 15.7494 16.6241H12.2494C12.0173 16.6241 11.7947 16.7163 11.6306 16.8803C11.4665 17.0444 11.3744 17.267 11.3744 17.4991V22.7492C11.3744 22.9812 11.2822 23.2038 11.1181 23.3679C10.954 23.5319 10.7315 23.6241 10.4995 23.6242L5.25011 23.6249C5.13519 23.6249 5.0214 23.6023 4.91523 23.5583C4.80906 23.5143 4.71258 23.4499 4.63132 23.3686C4.55006 23.2874 4.4856 23.1909 4.44162 23.0847C4.39764 22.9786 4.375 22.8648 4.375 22.7499V12.637C4.375 12.5151 4.40047 12.3945 4.44978 12.2831C4.4991 12.1716 4.57116 12.0716 4.66136 11.9896L13.4108 4.03424C13.5718 3.8878 13.7817 3.80665 13.9994 3.80664C14.2171 3.80663 14.4269 3.88777 14.588 4.0342L23.3386 11.9896C23.4288 12.0716 23.5009 12.1716 23.5502 12.2831C23.5995 12.3946 23.625 12.5151 23.625 12.637V22.7499C23.625 22.8648 23.6024 22.9786 23.5584 23.0847C23.5144 23.1909 23.4499 23.2874 23.3687 23.3686C23.2874 23.4499 23.1909 23.5143 23.0848 23.5583C22.9786 23.6023 22.8648 23.6249 22.7499 23.6249L17.4992 23.6242C17.2672 23.6241 17.0447 23.5319 16.8806 23.3679C16.7165 23.2038 16.6244 22.9812 16.6244 22.7492Z"
                    fill="#5E5E5E"
                  />
                  <path
                    d="M16.6244 22.7492V17.4991C16.6244 17.267 16.5322 17.0444 16.3681 16.8803C16.204 16.7163 15.9814 16.6241 15.7494 16.6241H12.2494C12.0173 16.6241 11.7947 16.7163 11.6306 16.8803C11.4665 17.0444 11.3744 17.267 11.3744 17.4991V22.7492C11.3744 22.9812 11.2822 23.2038 11.1181 23.3679C10.954 23.5319 10.7315 23.6241 10.4995 23.6242L5.25011 23.6249C5.13519 23.6249 5.0214 23.6023 4.91523 23.5583C4.80906 23.5143 4.71258 23.4499 4.63132 23.3686C4.55006 23.2874 4.4856 23.1909 4.44162 23.0847C4.39764 22.9786 4.375 22.8648 4.375 22.7499V12.637C4.375 12.5151 4.40047 12.3945 4.44978 12.2831C4.4991 12.1716 4.57116 12.0716 4.66136 11.9896L13.4108 4.03424C13.5718 3.8878 13.7817 3.80665 13.9994 3.80664C14.2171 3.80663 14.4269 3.88777 14.588 4.0342L23.3386 11.9896C23.4288 12.0716 23.5009 12.1716 23.5502 12.2831C23.5995 12.3946 23.625 12.5151 23.625 12.637V22.7499C23.625 22.8648 23.6024 22.9786 23.5584 23.0847C23.5144 23.1909 23.4499 23.2874 23.3687 23.3686C23.2874 23.4499 23.1909 23.5143 23.0848 23.5583C22.9786 23.6023 22.8648 23.6249 22.7499 23.6249L17.4992 23.6242C17.2672 23.6241 17.0447 23.5319 16.8806 23.3679C16.7165 23.2038 16.6244 22.9812 16.6244 22.7492V22.7492Z"
                    stroke="#5E5E5E"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
                <div className="text-xl text-black-dark">Tenant</div>
              </div>
            </div>
          </div>
          <div className="mb-5">
            <div className="h-nav bg-white border rounded-12 border-gray-lightest w-65">
              <div className="flex items-center h-full ml-5">
                <div className="text-xl text-black-dark">
                  Fire &amp; Burglary
                </div>
              </div>
            </div>
          </div>
          <div className="mb-5">
            <div className="h-nav bg-white border rounded-12 border-gray-lightest w-65">
              <div className="flex items-center h-full ml-5">
                <div className="text-xl text-black-dark">
                  Home Owner
                </div>
              </div>
            </div>
          </div>
          <div className="mb-5">
            <div className="h-nav bg-white border rounded-12 border-gray-lightest w-65">
              <div className="flex items-center h-full ml-5">
                <div className="text-xl text-black-dark">
                  Personal Accident
                </div>
              </div>
            </div>
          </div>
          <div className="mb-5">
            <div className="h-nav bg-white border rounded-12 border-gray-lightest w-65">
              <div className="flex items-center h-full ml-5">
                <div className="text-xl text-black-dark">
                  Professional Indemnity
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </>
  );
}

export default SelectInssurance;
