module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {  
    extend: {
      colors: {
      gray: {
        darkest: '#424242',
        dark: '#828282',
        heirs: '#5E5E5E',
        light: '#F8F8F8',
        lightest:'#e0e0e0',
        bd: '#bdbdbd',
      },
      black: {
        light: '#A6A6A6',
        dark: '#5E5E5E',
        dim: '#f2f2f2',
        heirs: '#1B1C1E',
      },
      pink: {
        light: '#FEF5F5',
        dark: '#FDE1E1'
      },
      heirsred: '#E90606',
      'heirsred-40': '#F69B9B',
      outline: '#C8C8C8',
      white: '#ffffff',
      aa: '#a9a9a9',
    },
    fontFamily: {
      roboto: 'Roboto',
    },
    height: {
      nav: '3.75rem',
    },
    width: {
      '65': '65%',
      '25': '25rem',
      fit: 'fit-content',
    },
    margin: {
      '22': '5.5rem',
    },
    borderRadius: {
      '12': '12px',
    },
    fontSize: {
      '3xxl': '2rem',
      '22': '1.3rem',
      xsm: '.6rem',
    },

    screens: {
      'xsm': {'max': '630px'},
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',
      '2xl': '1536px',
    },
  },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}